import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Field, reduxForm } from 'redux-form';

import FormInput from '../../components/FormInput';
import Button from '../../components/Button';
import {
  REQUIRED, TOO_LONG, TOO_SHORT, NOT_MATCHING
} from '../../model/ValidationErrors';

const UsernameInput = props => (
  <FormInput label="Username" placeholder="Type your username..." {...props} />
);

const PasswordInput = props => (
  <FormInput isSecure label="Password" placeholder="Type your password..." {...props} />
);

const ConfirmationInput = props => (
  <FormInput isSecure label="Confirmation" placeholder="Confirm your password..." {...props} />
);

class SignUpForm extends React.PureComponent {
  render() {
    const { handleSubmit } = this.props;

    return (
      <View style={styles.container}>
        {/* put fields here */}
        <Button title="Sign up!" onPress={handleSubmit} />
      </View>
    );
  }
}

export default SignUpForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  title: {
    textAlign: 'center',
    fontSize: 18,
    marginTop: 20,
    fontWeight: 'bold'
  },
  subtitle: {
    textAlign: 'center',
    fontSize: 14,
    marginTop: 8,
    marginBottom: 20
  },
  noAccount: {
    textAlign: 'center'
  }
});
