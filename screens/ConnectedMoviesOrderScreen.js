import { connect } from 'react-redux';
import { completeOrder, removeMovie } from '../redux/actionCreators/orderActionCreators';

import MoviesOrderScreen from './presentational/MoviesOrderScreen';

export default MoviesOrderScreen;
