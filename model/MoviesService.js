/* eslint-disable class-methods-use-this */
import genres from '../assets/data/genres';
import movies from '../assets/data/movies';

import calculatePrice from './PriceCalculator';

export default class MovieService {
  async fetchMovies() {
    return movies
      .filter(item => item.genres.length > 0 && item.year > 1990)
      .map((item, index) => ({ id: String(index), ...item, price: calculatePrice(item) }));
  }

  async fetchGenres() {
    return genres;
  }
}
