import { ADD_MOVIE_TO_ORDER, CONFIRM_ORDER, REMOVE_MOVIE_FROM_ORDER } from '../actionCreators/orderActionCreators';
import { LOGOUT } from '../actionCreators/accountActionCreators';

const initialState = { movies: [], totalPrice: 0 };

const currentOrderReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_MOVIE_TO_ORDER:
      return initialState;
    case REMOVE_MOVIE_FROM_ORDER:
      return initialState;
    case CONFIRM_ORDER:
      return initialState;
    case LOGOUT:
      return initialState;
    default:
      return state;
  }
};

export default currentOrderReducer;
